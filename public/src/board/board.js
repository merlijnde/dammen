import AbsoluteDiv from "../common/absoluteDiv.js"
import Pieces from "./pieces/pieces.js";
import Tiles from "./tiles/tiles.js";
import parseBoardConfig from "../common/parseBoardConfig.js";

class Board extends AbsoluteDiv {
    constructor(size) {
        super();
        this.size = size;
        this.className = 'board';
        this.pieces = new Pieces();
        this.tiles = new Tiles();

        // je moet dit doen omdat anders this niet werkt als je render aanroept van buitenaf (callingScope)
        this.render = this.render.bind(this);
        this.init = this.init.bind(this);
    }

    init(boardConfig) {
        const initialLayout = parseBoardConfig(boardConfig);
        const tileSize = this.size / initialLayout.length;

        initialLayout.forEach((row, y) => { //arrow function behoud de this scope
            row.forEach((cell, x) => {
                if (cell !== " ") {
                    const tile = this.tiles.newTile(String.fromCharCode(65 + x), y + 1)
                    tile.resizeTo(tileSize);
                    tile.moveTo(x * tileSize, y * tileSize);
                    switch (cell) {
                        case "w":
                        case "W":
                        case "b":
                        case "B": {
                            const piece = this.pieces.newPiece(cell)
                            piece.resizeTo(tileSize*.7);
                            piece.moveTo(x * tileSize + tileSize *.15, y * tileSize + tileSize * .15);
                            break;
                        }
                    }
                }
            });
        })
    }

    render(board) {
        super.render(board);
        Object.values(this.tiles.items).forEach((tile) => {
            tile.render(this.element);
        })
        Object.values(this.pieces.items).forEach((piece) => {
            piece.render(this.element);
        })
    }
}

export default Board





