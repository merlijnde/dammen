import Tile from "./tile.js";


/*
Deze class bevat alle zwart en witte stenen
 */
class Tiles {
    constructor(amount, size){
        this.items = {};

        this.render= this.render.bind(this);
    }

    render(parentElement) {
        this.blackPieces.forEach((piece, index) => {
            piece.left = index * 80
            piece.top = index * 80
            piece.render(parentElement)
        })

    }

    newTile(columnName, rowName) {
        const tile = new Tile(columnName, rowName);
        // voeg tile toe aan de tiles verzameling op basis van zijn id
        this.items[tile.id] = tile;
        return tile;
    }
}

export default Tiles;