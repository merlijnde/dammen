import AbsoluteDiv from "../../common/absoluteDiv.js";

class Tile extends AbsoluteDiv {
constructor(columnName, rowName) {
    super();
    this.className="tile";
    this.columnName = columnName;
    this.rowName = rowName;
    this.id = columnName+rowName;

    this.render = this.render.bind(this);
}

    render(parentElement) {
        super.render(parentElement);
    }
}

export default Tile