import Piece from "./piece.js";

class WhitePiece extends Piece {

    constructor(kind) {
        super();
        this.className += " white-piece"
        if (kind === "W"){
            this.promote()
        }
    }
}
export default WhitePiece;