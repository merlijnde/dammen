import AbsoluteDiv from "../../common/absoluteDiv.js";

class Piece extends AbsoluteDiv {
constructor(size) {
    super(size);
    this.className="piece";
    // bereken een random rotatie om het echt te laten lijken)
    this.render = this.render.bind(this);
    this.promote = this.promote.bind(this);
}

    render(parentElement) {
        super.render(parentElement);
     }

    promote() {
        this.queen = true;
        this.className += " queen"
    }
}

export default Piece