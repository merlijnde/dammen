import BlackPiece from "./blackPiece.js";
import WhitePiece from "./whitePiece.js";

/*
Deze class bevat alle zwart en witte stenen
 */
class Pieces {
    constructor() {
        this.items = [];
        this.newPiece = this.newPiece.bind(this);
    }


    newPiece(kind) {
        let result;
        switch (kind) {
            case "b" :
            case "B" :
                result = new BlackPiece(kind);
                break;
            case "w" :
            case "W" :
                result = new WhitePiece(kind);
                break;
        }
        this.items.push(result);
        return result;
    };
}

export default Pieces;