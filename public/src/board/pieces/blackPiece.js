import Piece from "./piece.js";

class BlackPiece extends Piece {

    constructor(kind) {
        super();
        this.className += " black-piece"
        if (kind === "B"){
            this.promote()
        }
    }
}
export default BlackPiece;