
/*
Leest de config string uit en splits deze op in tegels en stenen met de juiste coördinaten
de tegels worden op het bord geplaatst en de stenen op de bijbehorende tegels
de tegels worden tevens voorzien van coördinaten
 */
const parseConfig = function(boardConfig){
   // maak de markdown table leesbaar voor verwerking als een array
   // knip de rijen op per regel
   let rows = boardConfig.split('\n');
   // dit zorgt ervoor dat de headerrows niet geinterpreteerd worden.
   rows = rows.slice(2);
   // Loopt door elke regel van de array heen en vervangt elke regel met het resultaat van de functie
   rows = rows.map(function (row) {
         row = row.trim() // Dit haalt de blank space weg
             .slice(1, -1) // Haal de | aan het begin en einde weg
             .split('|')  // splitst de rij in cellen
             .slice(1); // gooit de eerste cel weg
         return row;
   });
   return rows;
}
export default parseConfig;