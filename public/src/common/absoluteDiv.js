class AbsoluteDiv {

    constructor() {
        this.element = undefined;
        this.className = undefined;
        this.render = this.render.bind(this);
        this.moveTo = this.moveTo.bind(this);
        this.resizeTo = this.resizeTo.bind(this);

    }

    moveTo(x,y){
        this.left = x;
        this.top = y;
        if (this.element) {
            this.element.style.top = this.top + "px";
            this.element.style.left = this.left + "px";
        }
    }

    resizeTo(size){
        this.size=size;
        if (this.element) {
            this.element.style.width = this.size + "px";
            this.element.style.height = this.size + "px";
        }
    }

    render(parentElement) {
        if (!this.element) {
            this.element = document.createElement('div');
            this.element.className = this.className;
            parentElement.appendChild(this.element);
        }

        this.element.style.top = this.top + "px";
        this.element.style.left = this.left + "px";
        this.element.style.width = this.size + "px";
        this.element.style.height = this.size + "px";
    }
}

export default AbsoluteDiv;