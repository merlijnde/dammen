let board8x8 =
   `| |a|b|c|d|e|f|g|h|
    |-|-|-|-|-|-|-|-|-|
    |1| |w| |w| |w| |w|
    |2|w| |w| |w| |w| |
    |3| |w| |w| |w| |w|
    |4|.| |.| |.| |.| |
    |5| |.| |.| |.| |.|
    |6|b| |b| |b| |b| |
    |7| |b| |b| |b| |b|
    |8|b| |b| |b| |b| |`




let board10x10 =
   `| |a|b|c|d|e|f|g|h|i|j|
    |-|-|-|-|-|-|-|-|-|-|-|
    |1| |w| |w| |w| |w| |w|
    |2|w| |w| |w| |w| |w| |
    |3| |w| |w| |w| |w| |w|
    |4|w| |w| |w| |w| |w| |
    |5| |.| |.| |.| |.| |.|
    |6|.| |.| |.| |.| |.| |
    |7| |b| |b| |b| |b| |b|
    |8|b| |b| |b| |b| |b| |
    |9| |b| |b| |b| |b| |b|
   |10|b| |b| |b| |b| |b| |`

let scenario1 =
    `| |a|b|c|d|e|f|g|h|
    |-|-|-|-|-|-|-|-|-|
    |1| |w| |w| |w| |w|
    |2|w| |w| |w| |w| |
    |3| |w| |w| |w| |w|
    |4|.| |w| |.| |w| |
    |5| |b| |.| |.| |.|
    |6|.| |b| |b| |b| |
    |7| |b| |b| |b| |b|
    |8|b| |b| |b| |b| |`



export default { board8x8, board10x10, scenario1 };