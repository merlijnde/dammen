import Board from "./board/board.js";
import boards from "./config/boards.js";

function main() {
    const size = 600;

    const app = window.document.getElementById('app');
    app.style.width = size + "px";
    app.style.height = size + "px";


// laad het bord
    let board = new Board(size);
    board.init(boards.scenario1);

// laad de spelregels
// let rules = new Rules(board, pieces);
// teken het bord
    board.render(app);


// begin het spel
//rules.startGame();

}

main();
