# Sources

## Markdown
Documentatie is in markdown en mermaid

- https://docs.gitlab.com/ee/user/markdown.html

- https://mermaid-js.github.io/mermaid/#/


## Gitlab pages
De pagina wordt gehost mbv gitlab pages
Om dit werkend te krijgen moet je een ```gitlab-ci.yml``` bestandje aanmaken en in de root van je project zetten.
Ik hiervoor het voorbeeld van het project https://gitlab.com/pages/plain-html gebruikt 

